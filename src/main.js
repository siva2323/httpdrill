
const fs = require('fs');
const http = require('http');
const uuid = require('uuid');
const port = 8000;


function read(path){
    return new Promise((resolve, reject) => {
        fs.readFile(path, "utf-8", (error, data) => {
            if(error){
                reject(error);
            }
            else {
                resolve(data);
            }
        });
    });
}

const server = http.createServer(requestListener);
server.listen(port, () => {
    console.log(`Server is listening on port: ${port}`);
});



function requestListener(request, response){
    
    if (request.url === "/html" && request.method === "GET"){
        read('index.html')
        .then((data) => { 
            response.writeHead(200);
            response.write(data);
            response.end();
        })
        .catch((error) => {
            console.error(error);
            response.writeHead(500);
            response.end("Error : reading html file");
        });
    }

    else if (request.url === "/json" && request.method === "GET"){
        read('data.json')
        .then((data) => {
            response.writeHead(200, {"Content-Type" : "application/json"});
            response.write(data);
            response.end();
        })
        .catch((error) => {
            console.error(error);
            response.writeHead(500);
            response.end("error : reading json data file");
        });
    }

    else if (request.url === "/uuid" && request.method === "GET"){
        response.writeHead(200, {"Content-Type" : "application/json"});
        response.write(JSON.stringify(`{ uuid : ${uuid.v4()} }`));
        response.end();
    }

    else if (request.url.startsWith("/status") && request.url.split("/").length === 3 && request.method === "GET"){

        let code = request.url.split("/").slice(-1);
        let message = {};
        if(code in http.STATUS_CODES){    
            message = {
                [code] : http.STATUS_CODES[code]
            };
        } 
        else {
            code = 404;
            message = {
                "error" : "Not a valid http status code"
            }
        }   
        response.writeHead(code, {"Content-Type" : "application/json"}); 
        response.write(JSON.stringify(message));
        response.end(); 
    } 

    else if (request.url.startsWith("/delay") && request.url.split("/").length === 3 && request.method === "GET"){
        
        let delay = Number(request.url.split("/").slice(-1));

        if(isNaN(delay) || delay < 0){
            response.writeHead(400, {"Content-Type" : "application/json"});
            response.end(JSON.stringify({
                "error" : "Not a valid time"
            }));
        }
        else {
            setTimeout(() => {
                response.writeHead(404); 
                response.write("delayed in : "+delay+" seconds")
                response.end();
            }, delay * 1000);
        }     
    }

    else {
        response.writeHead(404, {"Content-Type" : "application/json"});
        response.write(JSON.stringify({
            404 : http.STATUS_CODES[404]
        }));
        response.end();
    }
}
